from django import forms

class login_form(forms.Form):
	no_ktp = forms.CharField(label='No KTP', required=True, max_length=30, widget=forms.TextInput())
	email = forms.CharField(label='Email', required=True, max_length=30, widget=forms.TextInput())

class register_form(forms.Form):
	no_ktp = forms.CharField(label='No KTP', required=True, max_length=30, widget=forms.TextInput())
	nama = forms.CharField(label='Nama Lengkap', required=True, max_length=30, widget=forms.TextInput())
	email = forms.CharField(label='Email', required=True, max_length=30, widget=forms.TextInput())
	tanggal = forms.CharField(label='Tanggal Lahir', required=True, max_length=30, widget=forms.TextInput())
	nomor = forms.CharField(label='Nomor Telepon', required=True, max_length=30, widget=forms.TextInput())
	alamat = forms.CharField(label='Alamat', required=True, max_length=30, widget=forms.TextInput())