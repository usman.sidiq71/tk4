from django.shortcuts import render, redirect
from .models import login, register
from .forms import login_form, register_form

# Create your views here.
response = {}
def tes(request):
	return render(request, "page1.html")
def tes2(request):
	form = login_form(request.POST)
	if(request.method=='POST' and form.is_valid()):
		model = login(
				no_ktp = request.POST['no_ktp'],
				email= request.POST['email'])
		model.save()
	return redirect("rendertes2")
def rendertes2(request):
	response['form'] = login_form
	return render(request, "page2.html", response)
def tes3(request):
	form2 = register_form(request.POST)
	if(request.method=='POST' and form2.is_valid()):
		model = register(
				no_ktp = request.POST['no_ktp'],
				nama = request.POST['nama'],
				email = request.POST['email'],
				lahir = request.POST['lahir'],
				nomor = request.POST['nomor'],
				alamat = request.POST['alamat'])
		model.save()
	return redirect("rendertes3")
def rendertes3(request):
	response['form2'] = register_form
	return render(request, "page3.html", response)
