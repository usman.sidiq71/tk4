from django.urls import path
from .views import penugasan

urlpatterns = [
	path('penugasan/', penugasan, name='penugasan'),
]